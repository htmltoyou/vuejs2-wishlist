const functions = require('firebase-functions');
const ogs = require('open-graph-scraper');

exports.grab = functions.https.onRequest((req, res) => {
  if(req.query) {
    if(req.query.url === undefined) {
      res.send({message: "url cannot be undefined"});
    }
    const options = {
      'url': req.query.url
    };
    ogs(options)
      .then(result => {
        console.log(JSON.stringify(result))
        res.send(JSON.stringify(result))
        return true
      })
      .catch( error => {
        res.send(JSON.stringify(error))
      });
  }
});
