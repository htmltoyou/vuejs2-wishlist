import Vue from 'vue'
import {store} from '@/stores'
import Firebase from 'firebase'
import IScrollView from 'vue-iscroll-view'
import IScroll from 'iscroll'
import SocialSharing from 'vue-social-sharing'
import {
  Vuetify,
  VApp,
  VNavigationDrawer,
  VFooter,
  VList,
  VBtn,
  VIcon,
  VGrid,
  VToolbar,
  transitions,
  VAvatar,
  VDivider,
  VTextField,
  VCard,
  VTabs,
  VDataTable,
  VDialog,
  VRadioGroup,
  VTooltip,
  VSelect
} from 'vuetify'
import '../node_modules/vuetify/src/stylus/app.styl'

import App from './App'
import router from './router'

Vue.use(Vuetify, {
  components: {
    VApp,
    VNavigationDrawer,
    VFooter,
    VList,
    VBtn,
    VIcon,
    VGrid,
    VToolbar,
    transitions,
    VAvatar,
    VDivider,
    VTextField,
    VCard,
    VTabs,
    VDataTable,
    VDialog,
    VRadioGroup,
    VTooltip,
    VSelect
  }
})

Vue.use(IScrollView, IScroll)
Vue.use(SocialSharing)
Vue.config.productionTip = false

Firebase.initializeApp({
  apiKey: 'AIzaSyDfwrkbF3pvJYLK30Rax1cFCynKOV7102E',
  authDomain: 'wish-list-7f0dd.firebaseapp.com',
  databaseURL: 'https://wish-list-7f0dd.firebaseio.com',
  projectId: 'wish-list-7f0dd',
  storageBucket: 'wish-list-7f0dd.appspot.com',
  messagingSenderId: '44489835449'
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  created () {
    Firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        store.dispatch('setAuth', user)
      }
    })
  }
})
