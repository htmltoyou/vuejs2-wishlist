import Vue from 'vue'
import Router from 'vue-router'
import {store} from '@/stores'
import HomeComponent from '@/components/Home'
import LoginComponent from '@/components/Login'
import RegisterComponent from '@/components/Register'
import EditProfileComponent from '@/components/EditProfile'
import SearchComponent from '@/components/Search'
import UserComponent from '@/components/User'
import ListComponent from '@/components/List'
import CreateListComponent from '@/components/CreateList'
import PageNotFoundComponent from '@/components/PageNotFound'
import FriendsComponent from '@/components/Friends'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: HomeComponent
    },
    {
      path: '/login',
      name: 'Login',
      component: LoginComponent
    },
    {
      path: '/register',
      name: 'Register',
      component: RegisterComponent
    },
    {
      path: '/profile',
      name: 'Profile',
      component: EditProfileComponent
    },
    {
      path: '/search',
      name: 'Search',
      component: SearchComponent
    },
    {
      path: '/friends',
      name: 'Friends',
      component: FriendsComponent
    },
    {
      path: '/user/:id',
      name: 'User',
      component: UserComponent,
      props: true,
      beforeEnter: (to, from, next) => {
        store.dispatch('getFriendData', to.params.id).then(() => {
          next()
        }, error => {
          next(error)
        })
      }
    },
    {
      path: '/list/create',
      name: 'CreateList',
      component: CreateListComponent,
      props: true
    },
    {
      path: '/list/:id',
      name: 'List',
      component: ListComponent,
      beforeEnter: (to, from, next) => {
        store.dispatch('getListData', to.params.id).then(() => {
          next()
        }, error => {
          next(error)
        })
      }
    },
    {
      path: '*',
      name: 'PageNotFound',
      component: PageNotFoundComponent
    }
  ]
})
