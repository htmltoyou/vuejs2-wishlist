import Vue from 'vue'
import Vuex from 'vuex'
import {user} from './modules/user'
import {wishList} from './modules/wishList'
import {myList} from './modules/myList'
import {listData} from './modules/listData'
import {search} from './modules/search'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    user,
    wishList,
    myList,
    listData,
    search
  }
})
