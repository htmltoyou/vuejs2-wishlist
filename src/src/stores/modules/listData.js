import Vue from 'vue'
import Firebase from 'firebase'

export const listData = {
  state: {
    data: {
      title: '',
      ownerData: {},
      items: {},
      ownerUid: null,
      listId: null,
      reference: null
    }
  },
  getters: {
    getListData (state) {
      return state.data
    }
  },
  mutations: {
    setListData (state, listData) {
      Vue.set(state, 'data', listData)
      this.dispatch('getListOwner', listData.ownerUid)
    },
    setOwnerData (state, ownerData) {
      Vue.set(state.data, 'ownerData', ownerData)
    },
    addNewListItem (state, newItem) {
      Vue.set(state.data.items, newItem.uid, newItem.value)
    },
    editTitle (state, data) {
      Vue.set(state.data, 'title', data.newTitle.title)
      if (data.reference === 'wishLists') {
        this.dispatch('editWishListsTitle', data)
      }
    },
    deleteList (state, data) {
      Vue.set(state, 'data', {
        title: '',
        ownerData: {},
        items: {},
        ownerUid: null,
        listId: null,
        reference: null
      })
      if (data.reference === 'wishLists') {
        this.dispatch('deleteWishList', data.listId)
      }
    },
    reserveItem (state, data) {
      Vue.set(state.data.items, data.uid, data.item)
    },
    editItem (state, itemData) {
      Vue.set(state.data.items, itemData.uid, itemData.data.item)
    },
    deleteItemFromList (state, itemData) {
      Vue.delete(state.data.items, itemData.itemId)
    }
  },
  actions: {
    getListData ({commit}, listId) {
      let count = 0
      let childCount = 0
      return Firebase.database().ref().once('value').then(res => {
        childCount = res.numChildren()
        return new Promise((resolve, reject) => {
          Firebase.database().ref().on('child_added', snapshot => {
            let dataItem = snapshot.forEach((data) => {
              if (data.child(listId).exists()) {
                let newData = {
                  listId: listId,
                  reference: snapshot.key,
                  ownerUid: data.key,
                  title: data.child(listId).val().title,
                  items: data.child(listId).val().items || {}
                }
                commit('setListData', newData)
                resolve()
              }
            })
            count++
            if (childCount === count) {
              if (!dataItem) {
                let error = 'pagenotfound'
                reject(error)
              }
            }
          })
        })
      })
    },
    getListOwner ({commit}, ownerUid) {
      Firebase.database().ref('users/' + ownerUid).once('value', snapshot => {
        commit('setOwnerData', snapshot.val())
      })
    },
    addNewListItem ({commit}, newData) {
      let dataUrl = this.state.listData.data
      Firebase.database().ref(dataUrl.reference).child(dataUrl.ownerUid).child(dataUrl.listId).child('items').push(newData).then(snapshot => {
        let newListItem = {
          uid: snapshot.key,
          value: newData
        }
        commit('addNewListItem', newListItem)
      }).catch(error => {
        console.log(error.message)
      }).then(() => {
        this.dispatch('getUserWishLists')
      })
    },
    editTitle ({commit}, data) {
      const ref = Firebase.database().ref()
      const updateData = {}
      updateData['users/' + data.ownerUid + '/' + data.reference + '/' + data.listId] = data.newTitle.title
      updateData[data.reference + '/' + data.ownerUid + '/' + data.listId + '/title'] = data.newTitle.title
      ref.update(updateData, error => {
        if (error) {
          console.log(error.message)
        } else {
          commit('editTitle', data)
        }
      })
    },
    deleteList ({commit}, data) {
      const ref = Firebase.database().ref()
      const deleteData = {}
      deleteData['users/' + data.ownerUid + '/' + data.reference + '/' + data.listId] = null
      deleteData[data.reference + '/' + data.ownerUid + '/' + data.listId] = null
      ref.update(deleteData).then(() => {
        commit('deleteList', data)
      })
    },
    reserveItem ({commit}, data) {
      let dataUrl = this.state.listData.data
      Firebase.database().ref(dataUrl.reference).child(dataUrl.ownerUid).child(dataUrl.listId).child('items').child(data.itemId).update(data.item).then(snapshot => {
        commit('reserveItem', data)
      }).catch(error => {
        console.log(error.message)
      })
    },
    editItem ({commit}, itemData) {
      console.log(itemData.data)
      Firebase.database().ref(itemData.data.reference).child(itemData.data.ownerUid).child(itemData.data.listId).child('items').child(itemData.uid).update(itemData.data.item).then(snapshot => {
        commit('editItem', itemData)
      }).catch(error => {
        console.log(error.message)
      })
    },
    deleteItemFromList ({commit}, itemData) {
      Firebase.database().ref(itemData.reference).child(itemData.ownerUid).child(itemData.listId).child('items').child(itemData.itemId).set(null).then(snapshot => {
        commit('deleteItemFromList', itemData)
      }).catch(error => {
        console.log(error.message)
      })
    }
  }
}
