import Firebase from 'firebase'
import Vue from 'vue'

export const myList = {
  state: {
    myWishItemsCount: 0,
    myListItemsCount: 0,
    myLists: {
      title: '',
      items: {}
    }
  },
  getters: {
    getMyLists (state) {
      let list = {
        listId: Object.keys(state.myLists)[0],
        data: state.myLists
      }
      return list
    }
  },
  mutations: {
    addNewMyList (state, newMyList) {
      Vue.set(state.myLists, newMyList.uid, newMyList)
    },
    addMyLists (state, data) {
      Vue.set(state, 'myLists', data)
    },
    addNewItemToList (state, newItem) {
      Vue.set(state.myLists[newItem.listUid].items, newItem.itemUid, newItem.data)
    },
    editMyItem (state, itemData) {
      Vue.set(state.myLists[itemData.baseListUid].items, itemData.listUid, itemData.data)
    },
    deleteMyItem (state, itemData) {
      Vue.delete(state.myLists[itemData.listUid].items, itemData.itemUid)
    }
  },
  actions: {
    addNewMyList ({commit}, newMyList) {
      return new Promise((resolve, reject) => {
        Firebase.database().ref('myLists/' + this.state.user.uid).push(newMyList).then(response => {
          newMyList.uid = response.key
        }, error => {
          reject(error.message)
        }).then(() => {
          Firebase.database().ref('users/' + this.state.user.uid).child('myLists/' + newMyList.uid).set(newMyList.title).then(response => {
            commit('addNewMyList', newMyList)
            resolve()
          }, error => {
            reject(error.message)
          })
        })
      })
    },
    getUserMyLists ({commit}) {
      Firebase.database().ref('myLists/' + this.state.user.uid).once('value').then(response => {
        if (response.val() === null) {
          this.dispatch('addNewMyList', {
            title: 'My Items List',
            items: {}
          })
        } else {
          commit('addMyLists', response.val())
        }
      }, error => {
        console.log(error.message)
      })
    },
    addNewMyItem ({commit}, newListItemData) {
      Firebase.database().ref('myLists/' + this.state.user.uid).child(newListItemData.listUid).child('items').push(newListItemData.data).then(response => {
        newListItemData.itemUid = response.key
        commit('addNewItemToList', newListItemData)
      }, error => {
        console.log(error.message)
      })
    },
    editMyItem ({commit}, itemData) {
      const baseListUid = Object.keys(this.state.user.myLists)[0]
      Firebase.database().ref('myLists/' + this.state.user.uid).child(baseListUid).child('items').child(itemData.listUid).update(itemData.data).then(responce => {
        itemData.baseListUid = baseListUid
        commit('editMyItem', itemData)
      })
    },
    deleteMyItem ({commit}, itemUid) {
      const baseListUid = Object.keys(this.state.user.myLists)[0]
      Firebase.database().ref('myLists/' + this.state.user.uid).child(baseListUid).child('items').child(itemUid).set(null).then(responce => {
        const list = {
          listUid: baseListUid,
          itemUid: itemUid
        }
        commit('deleteMyItem', list)
      })
    }
  }
}
