import Firebase from 'firebase'
import Vue from 'vue'

export const search = {
  state: {
    searchResults: []
  },
  getters: {
    getSearchResults (state) {
      return state.searchResults
    }
  },
  mutations: {
    addSearchResults (state, searchResults) {
      state.searchResults.push(searchResults)
    },
    deleteSearchResults (state) {
      Vue.set(state, 'searchResults', [])
    }
  },
  actions: {
    searchUser ({commit}, data) {
      commit('deleteSearchResults')
      let type = ''
      if (data.indexOf('@') !== -1) {
        type = 'email'
      } else {
        type = 'lastName'
      }
      return new Promise((resolve, reject) => {
        Firebase.database().ref('users').orderByChild(type).equalTo(data).once('value').then(snapshot => {
          let value = snapshot.val()
          if (value) {
            snapshot.forEach((item) => {
              let data = {
                uid: item.key,
                data: item.val()
              }
              commit('addSearchResults', data)
            })

            resolve()
          } else {
            let error = 'User not found :('
            commit('deleteSearchResults')
            reject(error)
          }
        })
      })
    }
  }
}
