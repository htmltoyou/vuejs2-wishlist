import Vue from 'vue'
import Firebase from 'firebase'

export const user = {
  state: {
    firstName: '',
    lastName: '',
    email: '',
    photoUrl: 'http://spaceacre.co.za/wp-content/uploads/avatar-1.png',
    uid: null,
    authState: false,
    myWishItemsCount: 0,
    myListItemsCount: 0,
    wishLists: {},
    myLists: {},
    friends: {},
    friendData: {
      user: {
        firstName: '',
        lastName: '',
        email: '',
        photoUrl: 'http://spaceacre.co.za/wp-content/uploads/avatar-1.png',
        uid: null
      },
      wishLists: {},
      myLists: {}
    }
  },
  getters: {
    getUser (state) {
      return state
    },
    getAuthState (state) {
      return state.authState
    },
    getFriends (state) {
      return state.friends
    },
    getFriendById (state) {
      return state.friendData
    }
  },
  mutations: {
    addUserData (state, data) {
      state.firstName = data.firstName
      state.lastName = data.lastName
      state.email = data.email
      state.photoUrl = data.photoUrl
      state.friends = data.friends || {}
      state.wishLists = data.wishLists || {}
      state.myLists = data.myLists || {}
      state.friendData = data.friendData || {
        user: {
          firstName: '',
          lastName: '',
          email: '',
          photoUrl: 'http://spaceacre.co.za/wp-content/uploads/avatar-1.png',
          uid: null
        },
        wishLists: {},
        myLists: {}
      }
      this.dispatch('getUserWishLists')
    },
    setUserId (state, uid) {
      Vue.set(state, 'uid', uid)
      Vue.set(state, 'authState', true)
      this.dispatch('getUserData')
    },
    logout (state) {
      state.firstName = ''
      state.lastName = ''
      state.email = ''
      state.photoUrl = 'http://spaceacre.co.za/wp-content/uploads/avatar-1.png'
      state.uid = null
      state.authState = false
      state.wishLists = {}
      state.myLists = {}
      state.friends = {}
      state.friendData = {
        user: {
          firstName: '',
          lastName: '',
          email: '',
          photoUrl: 'http://spaceacre.co.za/wp-content/uploads/avatar-1.png',
          uid: null
        },
        wishLists: {},
        myLists: {}
      }
    },
    addToFriends (state, user) {
      let key = Object.keys(user)[0]
      Vue.set(state, 'friends' + [key], user[key])
    },
    deleteFromFriends (state, uid) {
      delete state.friends[uid]
      if (Object.keys(state.friends).length) {
        this.dispatch('getFriends')
      } else {
        Vue.set(state, 'friends', {})
      }
    },
    setFriends (state, data) {
      Vue.set(state, 'friends', data)
    },
    addFriendData (state, payload) {
      let data = {
        firstName: payload.data.firstName,
        lastName: payload.data.lastName,
        email: payload.data.email,
        photoUrl: payload.data.photoUrl,
        uid: payload.uid
      }
      Vue.set(state.friendData, 'user', data)
    },
    addFriendWishLists (state, wishLists) {
      Vue.set(state.friendData, 'wishLists', wishLists)
    },
    addFriendMyLists (state, myLists) {
      Vue.set(state.friendData, 'myLists', myLists)
    },
    changeUserData (state, newData) {
      Vue.set(state, 'firstName', newData.firstName)
      Vue.set(state, 'lastName', newData.lastName)
      Vue.set(state, 'email', newData.email)
    },
    changeAvatar (state, newPhotoUrl) {
      Vue.set(state, 'photoUrl', newPhotoUrl)
    }
  },
  actions: {
    register ({commit}, payload) {
      return new Promise((resolve, reject) => {
        Firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password).then(response => {
          commit('setUserId', response.uid)
        }, error => {
          reject(error.message)
        }).then(() => {
          let data = {
            firstName: payload.firstName,
            lastName: payload.lastName,
            email: payload.email,
            friends: {},
            wishLists: {},
            myLists: {},
            friendData: {
              user: {
                firstName: '',
                lastName: '',
                email: '',
                photoUrl: 'http://spaceacre.co.za/wp-content/uploads/avatar-1.png',
                uid: null
              },
              wishLists: {},
              myLists: {}
            },
            photoUrl: 'http://spaceacre.co.za/wp-content/uploads/avatar-1.png'
          }
          Firebase.database().ref('users/' + this.state.user.uid).set(data).then(() => {
            commit('addUserData', data)
            resolve()
          }, error => {
            reject(error.message)
          })
        })
      })
    },
    login ({commit}, payload) {
      return new Promise((resolve, reject) => {
        Firebase.auth().signInWithEmailAndPassword(payload.email, payload.password).then(response => {
          commit('setUserId', response.uid)
          resolve()
        }, error => {
          reject(error.message)
        })
      })
    },
    logout ({commit}, data) {
      Firebase.auth().signOut().then(() => {
        commit('logout')
      })
    },
    setAuth ({commit}, data) {
      commit('setUserId', data.uid)
    },
    getUserData ({commit}) {
      Firebase.database().ref('users/' + this.state.user.uid).once('value').then(response => {
        commit('addUserData', response.val())
      }, error => {
        console.log(error)
      })
    },
    getFriendData ({commit}, id) {
      Firebase.database().ref('users/' + id).once('value').then(response => {
        commit('addFriendData', {
          uid: response.key,
          data: response.val()
        })
      }, error => {
        console.log(error)
      }).then(() => {
        this.dispatch('getFriendWishLists', id)
      }).then(() => {
        this.dispatch('getFriendMyLists', id)
      })
    },
    getFriendWishLists ({commit}, id) {
      Firebase.database().ref('wishLists/' + id).once('value').then(response => {
        commit('addFriendWishLists', response.val())
      }, error => {
        console.log(error)
      })
    },
    getFriendMyLists ({commit}, id) {
      Firebase.database().ref('myLists/' + id).once('value').then(response => {
        commit('addFriendMyLists', response.val())
      }, error => {
        console.log(error)
      })
    },
    addToFriends ({commit}, user) {
      Firebase.database().ref('users/' + this.state.user.uid + '/friends/' + Object.keys(user)).set(user[Object.keys(user)]).then(response => {
        this.dispatch('getFriends')
      }, error => {
        console.log(error)
      })
    },
    getFriends ({commit}) {
      Firebase.database().ref('users/' + this.state.user.uid + '/friends').once('value').then(response => {
        let data = response.val()
        commit('setFriends', data)
      }, error => {
        console.log(error)
      })
    },
    deleteFromFriends ({commit}, uid) {
      Firebase.database().ref('users/' + this.state.user.uid + '/friends').child(uid).remove().then(() => {
        commit('deleteFromFriends', uid)
      })
    },
    changePassword ({commit}, newPassword) {
      Firebase.auth().currentUser.updatePassword(newPassword).then(() => {
      }).catch(error => {
        console.log(error.message)
      })
    },
    changeUserData ({commit}, newData) {
      if (newData.email !== this.state.email) {
        Firebase.auth().currentUser.updateEmail(newData.email).then(() => {
        }).catch(error => {
          console.log(error.message)
        })
      }
      Firebase.database().ref('users/' + this.state.user.uid).update(newData).then(response => {
        commit('changeUserData', newData)
      })
    },
    deleteUser ({commit}) {
      Firebase.auth().currentUser.delete().then(() => {
        commit('logout')
      }).catch(error => {
        console.log(error.message)
      })
    },
    chengeAvatar ({commit}, newAvatarFile) {
      Firebase.storage().ref('avatars').put(newAvatarFile).then(response => {
        commit('changeAvatar', response.downloadURL)
      }, error => {
        console.log(error.message)
      })
    }
  }
}
