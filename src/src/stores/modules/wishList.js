import Firebase from 'firebase'
import Vue from 'vue'

export const wishList = {
  state: {
    myWishItemsCount: 0,
    myListItemsCount: 0,
    newListUid: null,
    wishLists: {}
  },
  getters: {
    getWishLists (state) {
      return state.wishLists
    }
  },
  mutations: {
    addNewWishList (state, newWIshList) {
      Vue.set(state.wishLists, newWIshList.uid, newWIshList)
      console.log(state.wishLists)
    },
    addWishLists (state, data) {
      Vue.set(state, 'wishLists', data)
      this.dispatch('getUserMyLists')
    },
    editWishListsTitle (state, data) {
      Vue.set(state.wishLists[data.listId], 'title', data.newTitle.title)
    },
    deleteWishList (state, data) {
      Vue.delete(state.wishLists, data)
    },
    moveToMyList (state, data) {
      Vue.delete(state.wishLists[data.listId].items, data.itemId)
    },
    copyWishList (state, data) {
      Vue.set(state.wishLists, data.listId, data.items)
    },
    copyToMyWishList (state, item) {
      Vue.set(state.wishLists[item.listUid], item.itemUid, item.data)
    }
  },
  actions: {
    addNewWishList ({commit}, newWishList) {
      return new Promise((resolve, reject) => {
        Firebase.database().ref('wishLists/' + this.state.user.uid).push(newWishList).then(response => {
          newWishList.uid = response.key
        }, error => {
          reject(error.message)
        }).then(() => {
          Firebase.database().ref('users/' + this.state.user.uid).child('wishLists/' + newWishList.uid).set(newWishList.title).then(response => {
            commit('addNewWishList', newWishList)
            resolve()
          }, error => {
            reject(error.message)
          })
        })
      })
    },
    getUserWishLists ({commit}) {
      Firebase.database().ref('wishLists/' + this.state.user.uid).once('value').then(response => {
        commit('addWishLists', response.val() || {})
      }, error => {
        console.log(error)
      })
    },
    editWishListsTitle ({commit}, data) {
      commit('editWishListsTitle', data)
    },
    deleteWishList ({commit}, data) {
      commit('deleteWishList', data)
    },
    moveToMyList ({commit}, data) {
      Firebase.database().ref('myLists/' + data.ownerUid + '/' + Object.keys(this.state.user.myLists)[0] + '/items/' + data.itemId).set(data.item).then(() => {
        commit('moveToMyList', data)
      }, error => {
        console.log(error.message)
      }).then(() => {
        this.dispatch('deleteItemFromList', data)
      }).then(() => {
        this.dispatch('getUserMyLists')
      })
    },
    copyWishList ({commit}, listData) {
      const data = {
        title: listData.title,
        items: listData.items
      }
      console.log(listData)
      Firebase.database().ref('wishLists/' + this.state.user.uid + '/' + listData.listId).set(data).then(() => {
        commit('copyWishList', listData)
      }, error => {
        console.log(error.message)
      })
    },
    copyToMyWishList ({commit}, itemData) {
      console.log(itemData)
      // const data = {
      //   title: listData.title,
      //   items: listData.items
      // }
      // Firebase.database().ref('wishLists/' + this.state.user.uid + '/' + data.listId).set(data).then(() => {
      //   commit('copyToMyWishList', data)
      // }, error => {
      //   console.log(error.message)
      // })
    }
  }
}
